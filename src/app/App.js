import React from 'react';

import Header from '../components/header/header';
import Home from '../components/home/home';
import Footer from '../components/footer/footer';

// mocks data
import { headerData } from '../mocks/header-data';
import { footerData } from '../mocks/footer-data';

function App() {
  return (
    <div className='App'>
      <Header headerData={headerData} />
      <Home />
      <Footer footerData={footerData} />
    </div>
  )
}

export default App;
