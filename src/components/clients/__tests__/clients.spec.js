import React from 'react';
import { render } from '@testing-library/react';

import Clients from '../clients';

const clientsDescriptionMock = {
  id: '01',
  title: 'Clients',
  description: 'Description for client with id 01.'
};

const clientsDataMock = [
  {
    id: 'deptAgencyA',
    image: 'deptAgencyALogo',
    alt: 'company logo Dept Agency A'
  },
  {
    id: 'deptAgencyB',
    image: 'deptAgencyBLogo',
    alt: 'company logo Dept Agency B'
  }
];

describe('<Clients/>', () => {
  it('renders content', () => {
    const { getByText, getAllByAltText } = render(<Clients clientsDescription={clientsDescriptionMock} clientsData={clientsDataMock} />)

    expect(getByText('Clients')).toBeInTheDocument()
    expect(getByText('Description for client with id 01.')).toBeInTheDocument()
    expect(getAllByAltText('company logo Dept Agency A')[0]).toBeInTheDocument()
    expect(getAllByAltText('company logo Dept Agency B')[0]).toBeInTheDocument()
  })
})
