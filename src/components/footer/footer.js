import React from 'react';

import arrowUp from '../../assets/images/long-arrow-alt-up-solid.svg';

import './footer.css';

const Footer = ({ footerData }) => {
  const scrollToTop = () => {
    window.scroll(0, 0);
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  };

  return (
    <footer className='footer'>
      <div className='footer__line'>
        <nav className='footer__menu'>
          <h3 className='footer__title'>{footerData.title}</h3>
          {/* menu data */}
          <ul className='footer__list' role='navigation'>
            {footerData.menu.map(item => (
              <li key={item.id} className='footer__menu-item'>
                <a href={item.url}>{item.name}</a>
              </li>
            ))}
          </ul>
          {/* social medias data */}
          <ul className='footer__list' role='navigation'>
            {footerData.socialMedias.map(item => (
              <li key={item.id} className='footer__social-item'>
                <a href={item.url}>
                  <img src={item.icon} alt={item.alt} />
                </a>
              </li>
            ))}
          </ul>
        </nav>
        <div className='footer__info-section' role='contentinfo'>
          <div className='footer__company-info'>
            <p>Chamber of Commerce: {footerData.chamberOfCommerce}</p>
            <p>VAT: {footerData.vat}</p>
            <p>Terms and conditions</p>
          </div>
          <div className='footer__company-info'>
            <p>&copy; 2020 Dept Agency</p>
          </div>
        </div>
      </div>
      <div className='footer__top-column'>
        <span className='screen-reader-only'>Clique here to scroll to top of page</span>
        <button
          aria-label='Scroll to top of page'
          tabIndex={0}
          onClick={() => scrollToTop()}
          className='footer__top-button'
        >
          <img src={arrowUp} alt='arrow up icon' />
          TOP
        </button>
      </div>
    </footer>
  )
}

export default Footer;
