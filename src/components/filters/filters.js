import React from 'react';

import { filterWorksData } from '../../mocks/works-data';
import { filterIndustriesData } from '../../mocks/industries-data';

import './filters.css';

const Filters = () => {
  return (
    <div className='filters'>
      {/* works filtered data */}
      <div className='filters__filter-works'>
        <label
          htmlFor='worksFilter'
          id='worksLabel'
          className='filters__label-works'
        >
          Show me
        </label>
        <select
          aria-labelledby='worksLabel'
          name='worksFilter'
          id='worksFilter'
          className='filters__select-works'
        >
          {filterWorksData.map(item => (
            <option key={item.id} value={item.value}>
              {item.name}
            </option>
          ))}
        </select>
      </div>

      {/* industries filtered data */}
      <div className='filters__filter-industries'>
        <label
          htmlFor='industriesFilter'
          id='industriesLabel'
          className='filters__label-industries'
        >
          in
        </label>
        <select
          aria-labelledby='industriesLabel'
          name='industriesFilter'
          id='industriesFilter'
          className='filters__select-industries'
        >
          {filterIndustriesData.map(item => (
            <option key={item.id} value={item.value}>
              {item.name}
            </option>
          ))}
        </select>
      </div>
    </div>
  )
}

export default Filters;
