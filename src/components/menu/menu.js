import React from 'react';

import useLockBodyScroll from '../../hooks/use-lock-body-scroll';
import Icon from '../../assets/images/caret-right-solid.svg';

import './menu.css';

const Menu = ({ headerData, onClose }) => {
  useLockBodyScroll();

  const closeMenuByKeyboard = event => {
    if (event.keyCode === 13 || event.keyCode === 32) {
      onClose();
    };
  };

  return (
    <div className='menu' aria-label='Press escape to close this window'>
      <div className='menu__container'>
        <a
          href='/'
          title='Dept Agency Logo'
          tabIndex={0}
          aria-label='Dept Agency Logo'
          className='menu__logo'
        >
          {headerData.logo}
        </a>
        <div
          tabIndex={0}
          onClick={onClose}
          onKeyDown={e => closeMenuByKeyboard(e)}
          aria-label='Close menu'
          className='menu__menu-icon'
        >
          <span className='screen-reader-only'>Clique here to close the menu</span>
          <div className='menu__close-button'>
            <div className='menu__close-icon-line1'></div>
            <div className='menu__close-icon-line2'></div>
          </div>
        </div>
      </div>
      <nav className='menu__content'>
        <ul className='menu__list' role='navigation'>
          {headerData.menuItems.map(item => (
            <li className='menu__items' key={item.id}>
              <a href={item.url}>
                <img
                  src={Icon}
                  alt='arrow icon'
                  className='menu__triangle-icon'
                />
                {item.name}
              </a>
            </li>
          ))}
        </ul>
      </nav>
      <div className='menu__landen'>
        <ul className='menu__landen-items'>
          <li>landen</li>
          {headerData.landenItems.map(item => (
            <li key={item.id}>
              <a href={item.url}>
                <img
                  src={Icon}
                  alt='arrow icon'
                  className='menu__triangle-icon'
                />
                {item.name}
              </a>
            </li>
          ))}
        </ul>
      </div>
      <div className='menu__social'>
        <ul className='menu__social-items'>
          {headerData.socialItems.map(item => (
            <li key={item.id}>
              <a href={item.url}>
                <img
                  src={Icon}
                  alt='arrow icon'
                  className='menu__triangle-icon'
                />
                {item.name}
              </a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default Menu;
