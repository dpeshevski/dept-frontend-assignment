import React, { useState } from 'react';

import Menu from '../menu/menu';
import Modal from '../modal/modal';

import './header.css';

const Header = ({ headerData }) => {
  const [openMenu, setOpenMenu] = useState(false);
  const [openModal, setOpenModal] = useState(false);

  const [menuClassName, setMenuClassName] = useState('header__container');

  let [lastScrollPosition, setLastScrollPosition] = useState(0);

  const openByKeyboard = (event, onOpen) => {
    if (event.keyCode === 13 || event.keyCode === 32) {
      onOpen();
    };
  }

  const openMenuByKeyboard = (e) => openByKeyboard(e, () => setOpenMenu(true));
  const openModalByKeyboard = (e) => openByKeyboard(e, () => setOpenModal(true));

  const closeMenuByKeyboard = () => {
    window.addEventListener('keydown', event => {
      if (event.keyCode === 27) {
        setOpenMenu(false);
      }
    });
  }

  closeMenuByKeyboard();

  const changeMenuBackground = scrollPosition => {
    scrollPosition < 300
      ? setMenuClassName('header__container')
      : setMenuClassName('header__container--scrolled');
  };

  window.addEventListener('scroll', () => {
    setLastScrollPosition(window.scrollY);
    changeMenuBackground(lastScrollPosition);
  });

  return (
    <header className='header'>
      <div className={menuClassName}>
        <a
          href='/'
          aria-label='Dept Agency Logo'
          title='Dept Agency Logo'
          tabIndex={0}
          className='header__logo'
          role='banner'
        >
          {headerData.logo}
        </a>
        <div
          aria-label='Open menu'
          onClick={() => setOpenMenu(true)}
          tabIndex={0}
          onKeyDown={e => openMenuByKeyboard(e)}
          className='header__menu'
        >
          <span className='screen-reader-only'>Clique here to open the menu</span>
          <p className='header__menu-text'>Menu</p>
          <div className='header__menu-icon'>
            <div className='header__menu-icon-line'></div>
            <div className='header__menu-icon-line'></div>
          </div>
        </div>
      </div>
      <section className='header__highlight'>
        <img
          src={headerData.mobile}
          alt={headerData.alt}
          className='header__mobile-image'
        />
        <img
          src={headerData.desktop}
          alt={headerData.alt}
          className='header__desktop-image'
        />
        <h1 className='header__title'>{headerData.title}</h1>
        <div className='header__button-container'>
          <span className='screen-reader-only'>Clique here to open more view cases</span>
          <button
            aria-label='Open view case'
            onClick={() => setOpenModal(true)}
            tabIndex={0}
            onKeyDown={e => openModalByKeyboard(e)}
            className='header__button'
          >
            {headerData.button}
          </button>
        </div>
      </section>

      {openMenu && (<Menu headerData={headerData} onClose={() => setOpenMenu(false)} />)}

      {openModal && (<Modal textContent='More view cases' onClose={() => setOpenModal(false)} />)}
    </header>
  )
}

export default Header;
