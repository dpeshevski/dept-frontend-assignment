export const testemonialData = {
  id: 1,
  author: 'MATTIJS TEN BRINK – CEO, TRANSAVIA',
  content:
    'Dept helped us tell our story through a bold new identity and a robust online experience. To the tune of 60% growth in online bookings in just one month.'
};
