import facebook from '../assets/images/icons/facebook-icon.png';
import twitter from '../assets/images/icons/twitter-icon.png';
import instagram from '../assets/images/icons/instagram-icon.png';

export const footerData = {
  id: 1,
  title: 'Dept',
  chamberOfCommerce: '63464101',
  vat: 'NL 8552.47.502.B01',
  menu: [
    {
      id: 1,
      name: 'Work',
      url: '/'
    },
    {
      id: 2,
      name: 'Services',
      url: '/'
    },
    {
      id: 3,
      name: 'Stories',
      url: '/'
    },
    {
      id: 4,
      name: 'About',
      url: '/'
    },
    {
      id: 5,
      name: 'Careers',
      url: '/'
    },
    {
      id: 6,
      name: 'Contact',
      url: '/'
    }
  ],
  socialMedias: [
    {
      id: 1,
      name: 'facebook',
      icon: facebook,
      alt: 'facebook icon',
      url: 'https://www.facebook.com/'
    },
    {
      id: 2,
      name: 'twitter',
      icon: twitter,
      alt: 'twitter icon',
      url: 'https://www.twitter.com/'
    },
    {
      id: 3,
      name: 'instagram',
      icon: instagram,
      alt: 'instagram icon',
      url: 'https://www.instagram.com/'
    }
  ]
};
