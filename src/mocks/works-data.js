import bolcom from '../assets/images/clients/bolcom.png';
import kempen from '../assets/images/clients/kempen.png';
import philips from '../assets/images/clients/philips.png';
import gemeentemuseum from '../assets/images/clients/gemeentemuseum.png';
import florensis from '../assets/images/clients/florensis.png';
import lightning from '../assets/images/clients/lightning.png';
import tui from '../assets/images/clients/tui.png';
import chocomel from '../assets/images/clients/chocomel.png';
import jbl from '../assets/images/clients/jbl.png';
import zalando from '../assets/images/clients/zalando.png';
import koninklijke from '../assets/images/clients/koninklijke-bibliotheek.png';
import liberty from '../assets/images/clients/liberty-global.png';
import arla from '../assets/images/clients/arla.png';

export const worksData = [
  {
    id: 1,
    image: bolcom,
    name: 'Bol.com',
    title: 'A summer island in the Netherlands',
    category: 'commerce',
    industry: 'fast moving consumer goods',
    alt: 'a group of eight people playing in a pond wearing swimwear'
  },
  {
    id: 2,
    image: kempen,
    name: 'Kempen',
    title: 'Not some average banking website',
    category: 'digital marketing',
    industry: 'finance & insurance',
    alt:
      'a person with a computer on his lap pointing at a paper in front of the two people'
  },
  {
    id: 3,
    image: philips,
    name: 'Philips',
    title: 'Beautiful design meets innovative technology',
    category: 'branding',
    industry: 'utilities',
    alt: 'a group of lamps and lamps of various shapes'
  },
  {
    id: 4,
    image: gemeentemuseum,
    name: 'Gemeentemuseum',
    title: 'A 100 years of Mondriaan & De Stijl',
    category: 'strategy',
    industry: 'non-profit',
    alt: 'a woman with her back in front of an abstract and colorful painting'
  },
  {
    id: 5,
    image: 'without',
    name: 'Microsoft',
    title:
      'Tapping into Ireland’s unique gaming culture and bringing a fresh flavour to their Xbox social media channels',
    category: 'technology',
    industry: 'b2b'
  },
  {
    id: 6,
    image: 'without',
    name: "O'Neill",
    title:
      "Integrating existing content into O'Neill's new e-commerce platform",
    category: 'design',
    industry: 'travel'
  },
  {
    id: 7,
    image: florensis,
    name: 'Florensis',
    title: 'Rethinking the entire online ecosystem',
    category: 'digital teams',
    industry: 'recruitment',
    alt: 'man on his back with a backpack in front of a white board'
  },
  {
    id: 8,
    image: lightning,
    name: 'Be Lighting',
    title: 'Delivering clarity on a global scale',
    category: 'advertising',
    industry: 'health',
    alt: 'a lamppost in front of several buildings'
  },
  {
    id: 9,
    image: tui,
    name: 'Tui',
    title: 'Swipe to find your next holiday destination',
    category: 'digital marketing',
    industry: 'education',
    alt: 'drawing of a man with cap on a blue background'
  },
  {
    id: 10,
    image: 'without',
    name: 'Butlins',
    title:
      'Enhacing customer experience for personalised holiday planning using real time data',
    category: 'branding',
    industry: 'health'
  },
  {
    id: 11,
    image: 'without',
    name: 'Vacanseselect',
    title:
      'Predicting booking behavior for holidays with smart data, voice search and machine learning',
    category: 'content',
    industry: 'media'
  },
  {
    id: 12,
    image: chocomel,
    name: 'Chocomel',
    title: 'A compaign for the limited edition letter packs',
    category: 'commerce',
    industry: 'health',
    alt: 'five packs of chocomel with a heart stamped on each'
  },
  {
    id: 13,
    image: jbl,
    name: 'JBL',
    title: 'Live like a champion with Jerome Booteng',
    category: 'marketing',
    industry: 'media',
    alt: 'a man wearing headphones and looking thoughtful'
  },
  {
    id: 14,
    image: zalando,
    name: 'Zalando',
    title: 'Innovative SEO and content strategy for Zalando',
    category: 'commerce',
    industry: 'recruitment',
    alt: 'man writing on a white board with another man beside'
  },
  {
    id: 15,
    image: koninklijke,
    name: 'Koninklijke Bibliotheek',
    title: 'The search for the most influential book ever',
    category: 'advertising',
    industry: 'utilities',
    alt: 'several books stacked on an orange background'
  },
  {
    id: 16,
    image: liberty,
    name: 'Liberty Gobal',
    title: 'Delivering complex commerce solutions',
    category: 'design',
    industry: 'non-profit',
    alt: 'data center used to house computer systems and associated components'
  },
  {
    id: 17,
    image: arla,
    name: 'Arla',
    title: 'Swipe to find your next holiday destination',
    category: 'digital teams',
    industry: 'recruitment',
    alt: 'six packs of Arla yogurt of various shapes with fruits around'
  }
];

export const filterWorksData = [
  {
    id: 1,
    name: 'all work',
    value: 'all-work'
  },
  {
    id: 2,
    name: 'digital teams',
    value: 'digital-teams'
  },
  {
    id: 3,
    name: 'strategy',
    value: 'strategy'
  },
  {
    id: 4,
    name: 'technology',
    value: 'technology'
  },
  {
    id: 5,
    name: 'design',
    value: 'design'
  },
  {
    id: 6,
    name: 'advertising',
    value: 'advertising'
  },
  {
    id: 7,
    name: 'digital marketing',
    value: 'digital-marketing'
  },
  {
    id: 8,
    name: 'data intelligence',
    value: 'data-intelligence'
  },
  {
    id: 9,
    name: 'branding',
    value: 'branding'
  },
  {
    id: 10,
    name: 'content',
    value: 'content'
  },
  {
    id: 11,
    name: 'commerce',
    value: 'commerce'
  },
  {
    id: 12,
    name: 'media',
    value: 'media'
  },
  {
    id: 13,
    name: 'marketing',
    value: 'marketing'
  }
];

