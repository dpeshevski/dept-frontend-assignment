export const filterIndustriesData = [
  {
    id: 1,
    name: 'all industries',
    value: 'all-industries'
  },
  {
    id: 2,
    name: 'recruitment',
    value: 'recruitment'
  },
  {
    id: 3,
    name: 'finance & insurance',
    value: 'finance-insurance'
  },
  {
    id: 4,
    name: 'utilities',
    value: 'utilities'
  },
  {
    id: 5,
    name: 'b2b',
    value: 'b2b'
  },
  {
    id: 6,
    name: 'travel',
    value: 'travel'
  },
  {
    id: 7,
    name: 'retail & fashion',
    value: 'retail-fashion'
  },
  {
    id: 8,
    name: 'data intelligence',
    value: 'data-intelligence'
  },
  {
    id: 9,
    name: 'health',
    value: 'health'
  },
  {
    id: 10,
    name: 'education',
    value: 'education'
  },
  {
    id: 11,
    name: 'media',
    value: 'media'
  },
  {
    id: 12,
    name: 'fast moving consumer goods',
    value: 'fast-moving-consumer-goods'
  },
  {
    id: 13,
    name: 'non profit',
    value: 'non-profit'
  }
];