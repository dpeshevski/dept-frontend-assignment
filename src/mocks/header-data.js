
import mobileImage from '../assets/images/mobile-header-image.png'
import desktopImage from '../assets/images/desktop-header-image.png'

export const headerData = {
  id: 1,
  title: 'Work',
  logo: 'Dept',
  button: 'View case',
  desktop: desktopImage,
  mobile: mobileImage,
  alt: 'Man on his back with a backpack in front of a white board',
  menuItems: [
    {
      id: 1,
      name: 'HOME',
      url: '/'
    },
    {
      id: 2,
      name: 'WERK',
      url: '/'
    },
    {
      id: 3,
      name: 'OVER',
      url: '/'
    },
    {
      id: 4,
      name: 'DIENSTEN',
      url: '/'
    },
    {
      id: 5,
      name: 'PARTNERS',
      url: '/'
    },
    {
      id: 6,
      name: 'STORIES',
      url: '/'
    },
    {
      id: 7,
      name: 'VACATURES',
      url: '/'
    },
    {
      id: 8,
      name: 'EVENTS',
      url: '/'
    },
    {
      id: 9,
      name: 'CONTACTS',
      url: '/'
    }
  ],
  landenItems: [
    {
      id: 1,
      name: 'global',
      url: '/'
    },
    {
      id: 2,
      name: 'nederland',
      url: '/'
    },
    {
      id: 3,
      name: 'united states',
      url: '/'
    },
    {
      id: 4,
      name: 'ireland',
      url: '/'
    },
    {
      id: 5,
      name: 'united kingdom',
      url: '/'
    },
    {
      id: 6,
      name: 'deutschland',
      url: '/'
    },
    {
      id: 7,
      name: 'schweiz',
      url: '/'
    }
  ],
  socialItems: [
    {
      id: 1,
      name: 'facebook',
      url: 'https://www.facebook.com/'
    },
    {
      id: 2,
      name: 'twitter',
      url: 'https://twitter.com/'
    },
    {
      id: 3,
      name: 'instagram',
      url: 'https://www.instagram.com/'
    },
    {
      id: 4,
      name: 'linkedin',
      url: 'https://www.linkedin.com/'
    }
  ]
};
