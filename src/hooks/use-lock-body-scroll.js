import { useLayoutEffect } from 'react';

const useLockBodyScroll = () => {
  useLayoutEffect(() => {
    const computedStyle = window.getComputedStyle(document.body).overflow;
    document.body.style.overflow = 'hidden';

    return () => (document.body.style.overflow = computedStyle);
  }, []);
};

export default useLockBodyScroll;
